package ru.shipova.tm.constant;

public enum RoleType {
    ADMIN("АДМИНИСТРАТОР"), USER("ОБЫЧНЫЙ ПОЛЬЗОВАТЕЛЬ");

    private final String name;

    RoleType(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }
}
