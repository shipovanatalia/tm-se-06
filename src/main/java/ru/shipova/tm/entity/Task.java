package ru.shipova.tm.entity;

import java.util.Date;

public class Task {
    private String id;
    private String projectId;
    private String name;
    private String description;
    private Date dateOfBegin;
    private Date dateOfEnd;
    private String userId;

    public Task(String id, String name, String projectId, String userId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateOfBegin() {
        return dateOfBegin;
    }

    public Date getDateOfEnd() {
        return dateOfEnd;
    }

    public String getUserId() {
        return userId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateOfBegin(Date dateOfBegin) {
        this.dateOfBegin = dateOfBegin;
    }

    public void setDateOfEnd(Date dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
