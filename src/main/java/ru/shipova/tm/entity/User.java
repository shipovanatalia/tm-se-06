package ru.shipova.tm.entity;

import ru.shipova.tm.constant.RoleType;

public class User {
    private String userId;
    private String login;
    private RoleType roleType;
    private String passwordHash;

    public User(String userId, String login, String passwordHash, RoleType roleType) {
        this.userId = userId;
        this.login = login;
        this.roleType = roleType;
        this.passwordHash = passwordHash;
    }

    public String getUserId() {
        return userId;
    }

    public String getLogin() {
        return login;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
