package ru.shipova.tm.bootstrap;

import ru.shipova.tm.api.ServiceLocator;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.command.project.ProjectClearCommand;
import ru.shipova.tm.command.project.ProjectCreateCommand;
import ru.shipova.tm.command.project.ProjectListCommand;
import ru.shipova.tm.command.project.ProjectRemoveCommand;
import ru.shipova.tm.command.system.ExitCommand;
import ru.shipova.tm.command.system.HelpCommand;
import ru.shipova.tm.command.task.*;
import ru.shipova.tm.command.user.*;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessErrorException;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.TerminalService;
import ru.shipova.tm.service.UserService;

import java.util.*;

/**
 * Класс загрузчика приложения
 */

public class Bootstrap implements ServiceLocator {

    private TaskRepository taskRepository = new TaskRepository();
    private ProjectRepository projectRepository = new ProjectRepository();
    private ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private TaskService taskService = new TaskService(taskRepository, projectRepository);
    private TerminalService terminalService = new TerminalService(this);
    private UserRepository userRepository = new UserRepository();
    private UserService userService = new UserService(userRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String commandName = command.getName(); //Command Line Interface
        final String cliDescription = command.getDescription();
        if (commandName == null || commandName.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    private void execute(final String commandName) throws Exception {
        if (commandName == null || commandName.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(commandName);
        if (abstractCommand == null) return;
        User currentUser = userService.getCurrentUser();


        if (currentUser == null){
            if (abstractCommand.needAuthorize()) try {
                throw new AccessErrorException();
            } catch (AccessErrorException e) {
                System.out.println("ACCESS ERROR");
            }
            if (!abstractCommand.needAuthorize()) abstractCommand.execute();
        } else {
            String role = currentUser.getRoleType().toString();

            if ("USER".equals(role)) {
                if (abstractCommand.isOnlyAdminCommand()) try {
                    throw new AccessErrorException();
                } catch (AccessErrorException e) {
                    System.out.println("ACCESS ERROR");
                }
                if (!abstractCommand.isOnlyAdminCommand()) abstractCommand.execute();
            }

            if ("ADMIN".equals(role)) {
                abstractCommand.execute();
            }
        }
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() throws Exception {
        loadCommands();

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        String commandName = "";
        while (!"exit".equals(commandName)) {
            commandName = terminalService.nextLine();
            if (!commands.containsKey(commandName)) {
                System.out.println("WRONG COMMAND. ENTER 'help' TO GET ALL AVAILABLE COMMANDS.");
            }
            execute(commandName);
        }
    }

    private void loadCommands() {
        try {
            registry(new HelpCommand());
            registry(new ExitCommand());
            registry(new ProjectListCommand());
            registry(new ProjectCreateCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectRemoveCommand());
            registry(new TaskClearCommand());
            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskRemoveCommand());
            registry(new TaskShowCommand());
            registry(new UserLoginCommand());
            registry(new UserLogoutCommand());
            registry(new UserRegistryCommand());
            registry(new UserSetPasswordCommand());
            registry(new UserProfileCommand());
            registry(new UserUpdateCommand());
        } catch (CommandCorruptException e) {
            e.printStackTrace();
        }
    }



    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() { return userService; }
}
