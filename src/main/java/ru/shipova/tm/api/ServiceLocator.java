package ru.shipova.tm.api;

import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.TerminalService;
import ru.shipova.tm.service.UserService;

public interface ServiceLocator {
    TerminalService getTerminalService();
    ProjectService getProjectService();
    TaskService getTaskService();
    UserService getUserService();
}
