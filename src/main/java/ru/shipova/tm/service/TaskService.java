package ru.shipova.tm.service;

import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

public class TaskService {
    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;

    public TaskService(TaskRepository taskRepository, ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public void create(String taskName, String projectName, String userId) throws ProjectDoesNotExistException {
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty()) return;

        String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        String taskId = UUID.randomUUID().toString();
        taskRepository.persist(new Task(taskId, taskName, projectId, userId));
    }

    public List<String> showAllTasksOfProject(String projectName) throws ProjectDoesNotExistException {
        if (projectName == null || projectName.isEmpty()) return null;

        String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        return taskRepository.showAllTasksOfProject(projectId);
    }

    public List<Task> getListTask(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    public void remove(String taskName, String userId) {
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        String taskId = taskRepository.getTaskIdByName(taskName);
        Task task = taskRepository.findOne(taskId);

        if (task == null) return;
        if (!userId.equals(task.getUserId())) return;

        taskRepository.remove(taskId);
    }
}
