package ru.shipova.tm.service;

import ru.shipova.tm.entity.Project;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void create(String projectName, String userId) {
        if (projectName == null || projectName.isEmpty()) return;
        String projectId = UUID.randomUUID().toString();
        projectRepository.persist(new Project(projectId, projectName, userId));
    }

    public List<Project> getListProject(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }

    public void remove(String projectName, String userId) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        String projectId = projectRepository.getProjectIdByName(projectName);

        Project project = projectRepository.findOne(projectId);
        if (project == null) return;
        if (!userId.equals(project.getUserId())) return;

        projectRepository.remove(projectId);
        taskRepository.removeAllTasksOfProject(projectId);
    }
}
