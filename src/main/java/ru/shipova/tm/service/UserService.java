package ru.shipova.tm.service;

import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.UUID;

public class UserService {

    private UserRepository userRepository;
    private User currentUser;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
        currentUser = null;
    }

    public void registryUser(String login, String password, String role) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;

        String passwordHash = PasswordHashUtil.md5(password);
        String userId = UUID.randomUUID().toString();

        RoleType roleType = getRoleType(role);
        userRepository.persist(new User(userId, login, passwordHash, roleType));
    }

    public void updateUser(User user, String role){
        if (role == null || role.isEmpty()) return;
        if (user == null) return;
        RoleType roleType = getRoleType(role);
        userRepository.updateUser(user, roleType);
    }

    private RoleType getRoleType(String role){
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }

    public void setNewPassword(String userLogin, String password) {
        if (password == null || password.isEmpty()) return;
        if (userLogin == null || userLogin.isEmpty()) return;
        String passwordHash = PasswordHashUtil.md5(password);
        userRepository.setNewPassword(userLogin, passwordHash);
    }

    public User authorize(String login, String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) return null;
        return userRepository.findByLogin(login);
    }

    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        User user = findByLogin(login);
        if (user == null) return false;
        String passwordHash = PasswordHashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
