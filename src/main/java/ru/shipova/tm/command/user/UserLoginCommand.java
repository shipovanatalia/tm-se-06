package ru.shipova.tm.command.user;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.UserService;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Login in task manager.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PASSWORD");
        String password = serviceLocator.getTerminalService().nextLine();
        UserService userService = serviceLocator.getUserService();
        User user = userService.authorize(login, password);
        if (user == null) {
            System.out.println("WRONG LOGIN OR PASSWORD");
            execute();
        }
        userService.setCurrentUser(user);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
