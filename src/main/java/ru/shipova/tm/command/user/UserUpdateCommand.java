package ru.shipova.tm.command.user;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.UserService;

public class UserUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update role type of user.";
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE]");
        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();

        System.out.println("ENTER LOGIN OF USER TO UPDATE:");
        String userLogin = serviceLocator.getTerminalService().nextLine();
        User userToUpdate = userService.findByLogin(userLogin);

        System.out.println("ENTER NEW ROLE TYPE:");
        String newRoleType = serviceLocator.getTerminalService().nextLine();
        userService.updateUser(userToUpdate, newRoleType);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
