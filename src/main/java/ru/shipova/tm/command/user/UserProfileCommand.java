package ru.shipova.tm.command.user;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.UserService;

public class UserProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Get all information about user.";
    }

    @Override
    public void execute() {
        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        if (currentUser == null) {
            System.out.println("PLEASE LOGIN TO GET INFORMATION");
            return;
        }
        System.out.println("[USER PROFILE]");
        System.out.println("USER LOGIN: " + currentUser.getLogin());
        System.out.println("USER ROLE TYPE: " + currentUser.getRoleType().displayName());
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
