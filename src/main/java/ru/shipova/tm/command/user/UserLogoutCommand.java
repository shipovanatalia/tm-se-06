package ru.shipova.tm.command.user;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.service.UserService;

public class UserLogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Sign out from Task Manager.";
    }

    @Override
    public void execute(){
        UserService userService = serviceLocator.getUserService();
        userService.setCurrentUser(null);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
