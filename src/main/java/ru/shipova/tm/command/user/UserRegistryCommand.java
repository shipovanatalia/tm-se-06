package ru.shipova.tm.command.user;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.service.UserService;

public class UserRegistryCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public String getDescription() {
        return "Registry new user.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY USER]");
        System.out.println("ENTER LOGIN");
        String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PASSWORD");
        String password = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER ROLE OF USER");
        String roleType = serviceLocator.getTerminalService().nextLine();
        UserService userService = serviceLocator.getUserService();
        userService.registryUser(login, password, roleType);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
