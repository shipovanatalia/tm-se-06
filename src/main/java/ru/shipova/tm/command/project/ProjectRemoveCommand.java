package ru.shipova.tm.command.project;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.UserService;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME OF PROJECT:");
        ProjectService projectService = serviceLocator.getProjectService();
        String projectName = serviceLocator.getTerminalService().nextLine();

        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();

        projectService.remove(projectName, userId);
        System.out.println("[PROJECT " + projectName + " REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
