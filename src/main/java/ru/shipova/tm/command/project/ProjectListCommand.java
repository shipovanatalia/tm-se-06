package ru.shipova.tm.command.project;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.UserService;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        ProjectService projectService = serviceLocator.getProjectService();
        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();

        int index = 1;
        for (Project project : projectService.getListProject(userId)) {
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println();
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
