package ru.shipova.tm.command.project;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.service.ProjectService;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        ProjectService projectService = serviceLocator.getProjectService();
        String projectName = serviceLocator.getTerminalService().nextLine();
        String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        projectService.create(projectName, userId);
        System.out.println("[OK]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }


}
