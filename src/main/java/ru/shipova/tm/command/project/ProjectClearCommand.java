package ru.shipova.tm.command.project;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.UserService;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        ProjectService projectService = serviceLocator.getProjectService();
        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();
        projectService.clear(userId);
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
