package ru.shipova.tm.command.task;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.UserService;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        TaskService taskService = serviceLocator.getTaskService();
        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();
        taskService.clear(userId);
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

}
