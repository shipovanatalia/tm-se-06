package ru.shipova.tm.command.task;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.UserService;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        TaskService taskService = serviceLocator.getTaskService();
        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();

        int index = 1;
        for (Task task : taskService.getListTask(userId)) {
            System.out.println(index++ + ". " + task.getName());
        }
        System.out.println();
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
