package ru.shipova.tm.command.task;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.UserService;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME OF TASK:");
        TaskService taskService = serviceLocator.getTaskService();
        String projectName = serviceLocator.getTerminalService().nextLine();

        UserService userService = serviceLocator.getUserService();
        User currentUser = userService.getCurrentUser();
        String userId = currentUser.getUserId();

        taskService.remove(projectName, userId);
        System.out.println("[TASK " + projectName + " REMOVED]");
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
