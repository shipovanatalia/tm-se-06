package ru.shipova.tm.command.task;

import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.service.TaskService;

public class TaskShowCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-show";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME OF PROJECT:");
        TaskService taskService = serviceLocator.getTaskService();
        String projectName = serviceLocator.getTerminalService().nextLine();
        System.out.println("PROJECT " + projectName + " CONTAINS TASKS:");
        try {
            for (String nameOfTask : taskService.showAllTasksOfProject(projectName)) {
            System.out.println(nameOfTask);}
        } catch (ProjectDoesNotExistException e) {
            System.out.println("PROJECT DOES NOT EXISTS");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
