package ru.shipova.tm.repository;

import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static ru.shipova.tm.constant.RoleType.ADMIN;
import static ru.shipova.tm.constant.RoleType.USER;

public class UserRepository {

    private Map<String, User> userMap = new HashMap<>();//key = userId

    public UserRepository() {
        String userId = UUID.randomUUID().toString();
        String userLogin = "user";
        String userPasswordHash = PasswordHashUtil.md5("user");
        userMap.put(userId, new User (userId,userLogin, userPasswordHash, USER));


        String adminId = UUID.randomUUID().toString();
        String adminLogin = "admin";
        String adminPasswordHash = PasswordHashUtil.md5("admin");
        userMap.put(adminId, new User(adminId, adminLogin, adminPasswordHash, ADMIN));
    }

    public User findByLogin(String login) {
        String userId = "";
        for (Map.Entry<String, User> entry : userMap.entrySet()) {
            if (login.equals(entry.getValue().getLogin())) {
                userId = entry.getKey();
            }
        }
        if (!userMap.containsKey(userId)) return null;
        return userMap.get(userId);
    }

    public void persist(User user) {
        if (isExist(user)) return;
        userMap.put(user.getLogin(), user);
    }

    private boolean isExist(User user) {
        String userId = user.getUserId();
        return userMap.containsKey(userId);
    }

    public void setNewPassword(String login, String passwordHash) {
        User user = findByLogin(login);
        if (!isExist(user)) return;

        for (Map.Entry<String, User> entry : userMap.entrySet()) {
            if (user.getUserId().equals(entry.getKey())) {
                entry.getValue().setPasswordHash(passwordHash);
            }
        }
    }

    public void updateUser(User user, RoleType roleType){
        if (!isExist(user)) return;
        for (Map.Entry<String, User> entry : userMap.entrySet()) {
            if (user.getUserId().equals(entry.getKey())) {
                entry.getValue().setRoleType(roleType);
            }
        }
    }
}
