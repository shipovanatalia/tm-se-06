package ru.shipova.tm.repository;

import ru.shipova.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projectMap = new HashMap<>();

    public ProjectRepository() {
    }

    public Project findOne(String projectId) {
        if (!projectMap.containsKey(projectId)) return null;
        return projectMap.get(projectId);
    }

    public List<Project> findAll() {
        List<Project> allProjects = new ArrayList<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            allProjects.add(entry.getValue());
        }
        return allProjects;
    }

    public List<Project> findAllByUserId(String userId) {
        List<Project> projects = new ArrayList<>();

        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())){
                projects.add(entry.getValue());
            }
        }
        return projects;
    }

    public String getProjectIdByName(String projectName) {
        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void persist(Project project) {
        if (isExist(project)) return;
        projectMap.put(project.getId(), project);
    }

    public void merge(Project project) {
        if (isExist(project)) {
            update(project);
        } else {
            projectMap.put(project.getId(), project);
        }
    }

    public void remove(String projectId) {
        projectMap.remove(projectId);
    }

    public void remove(Project project) {
        String projectId = project.getId();
        projectMap.remove(projectId);
    }

    public void removeAll(String userId) {
        List<Project> projectsToRemove = findAllByUserId(userId);
        for (Project project : projectsToRemove) {
            for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
                if (project.getId().equals(entry.getKey())) {
                    remove(project);
                }
            }
        }
    }

    private void update(Project project) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (project.getId().equals(entry.getKey())) {
                entry.getValue().setName(project.getName());
                entry.getValue().setDescription(project.getDescription());
                entry.getValue().setDateOfBegin(project.getDateOfBegin());
                entry.getValue().setDateOfEnd(project.getDateOfEnd());
            }
        }
    }

    private boolean isExist(Project project){
        String projectId = project.getId();
        return projectMap.containsKey(projectId);
    }
}
